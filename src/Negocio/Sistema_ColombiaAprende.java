/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.*;
import java.time.LocalTime;
import ufps.util.colecciones_seed.ColaP;
import ufps.util.colecciones_seed.ListaCD;
import ufps.util.colecciones_seed.Pila;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author madar
 */
public class Sistema_ColombiaAprende {

    private ListaCD<Notificacion> notificaciones;
    private Pila<Hardware_Academico>[] inventario;
    private ColaP<Usuario> usuarios;

    public Sistema_ColombiaAprende() {
        this.usuarios = new ColaP();
        this.inventario = new Pila[2];
        this.inventario[0] = new Pila(); //PC
        this.inventario[1] = new Pila(); //tablet
    }

    public void cargarUsuarios(String url) {

        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            String datos = v[i].toString();
            //CEDULA;Nombres;Correo;HORA DE REALIZACION DE REGISTRO(HH:MM:SS);cantidad_hijos_infantes;cantidad_hijos_adolecentes
            String datos2[] = datos.split(";");
            int cedula = Integer.parseInt(datos2[0]);
            String nombre = datos2[1];
            String email = datos2[2];
            LocalTime fecha = getFecha(datos2[3]);
            byte cI = Byte.parseByte(datos2[4]);
            byte cA = Byte.parseByte(datos2[5]);
            Usuario nuevo = new Usuario(cedula, nombre, email, fecha, cI, cA);
            this.usuarios.enColar(nuevo, nuevo.getPrioridad() * -1);
        }

        // :)
    }

    private LocalTime getFecha(String fecha) {
        String fecha2[] = fecha.split(":");
        return LocalTime.of(Integer.parseInt(fecha2[0]), Integer.parseInt(fecha2[1]), Integer.parseInt(fecha2[2]));

    }

    public void cargarHardware_Academico(String url) {

        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            //id_hardware;tipo;descripcion
            String datos = v[i].toString();
            String datos2[] = datos.split(";");
            byte id = Byte.parseByte(datos2[0]);
            boolean esPC = Boolean.parseBoolean(datos2[1]);
            String descripcion = datos2[2];
            Hardware_Academico nuevo = new Hardware_Academico(esPC, id, descripcion);
            int i2 = esPC ? 0 : 1;
            this.inventario[i2].apilar(nuevo);

        }
        // :)
    }

    public void procesar_Entregas_Hardware() {

        //:)
    }

    public String getListadoUsuarios() {
        String msg = "";
        ColaP<Usuario> respaldo = new ColaP();
        while (!this.usuarios.esVacia()) {
            Usuario x = this.usuarios.deColar();
            respaldo.enColar(x);
            msg += x.toString() + "\n";
        }
        while (!respaldo.esVacia()) {
            Usuario x = respaldo.deColar();

            this.usuarios.enColar(x, x.getPrioridad());
            //this.usuarios.enColar(x);
        }
        return msg;
    }

    public String getListadoHardware() {
        return this.getListadoPila(this.inventario[0]) + this.getListadoPila(this.inventario[1]);
    }

    /**
     * HAY QUE ARRGLARLO PRO QUE LA PILA SE PIERDE
     *
     * @param p
     * @return
     */
    private String getListadoPila(Pila<Hardware_Academico> p) {
        String msg = "";
        while (!p.esVacia()) {
            msg += p.desapilar().toString() + "\n";
        }
        return msg + "\n\n";
    }
}
