/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;

/**
 *
 * @author madar
 */
public class TestFechas {
    public static void main(String[] args) {
        LocalDate fechaNacimiento=LocalDate.of(1980, 3, 5);
        LocalDate actual=LocalDate.now();
        long edad=ChronoUnit.YEARS.between(fechaNacimiento,actual);
        System.out.println("Su edad es:"+edad);
        Period periodo=Period.between(fechaNacimiento, actual);
        System.out.printf("Tu edad es: %s años, %s meses y %s días\n",
                    periodo.getYears(), periodo.getMonths(), periodo.getDays());
        
        //Usando LocalTime:
        LocalTime tiempo=LocalTime.of(4, 30,58);
        System.out.println("Tiempo solo hora, min y segundos:"+tiempo.toString());
    }
}
