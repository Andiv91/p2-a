/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.time.LocalTime;

/**
 *
 * @author coloqué acá su nombres y código
 */
public class Usuario {

    private int cedula;
    private String nombre;
    private String email;
    //Ver información de la clase LocalTime: https://devs4j.com/2018/10/30/java-8-manejo-de-fechas-y-tiempo-localdate-localtime-y-localdatetime/
    private LocalTime fechaDeRegistro;
    private byte cantInfantes;
    private byte cantAdolencentes;

    public Usuario() {
    }

    public Usuario(int cedula, String nombre, String email, LocalTime fechaDeRegistro, byte cantInfantes, byte cantAdolencentes) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.email = email;
        this.fechaDeRegistro = fechaDeRegistro;
        this.cantInfantes = cantInfantes;
        this.cantAdolencentes = cantAdolencentes;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalTime getFechaDeRegistro() {
        return fechaDeRegistro;
    }

    public void setFechaDeRegistro(LocalTime fechaDeRegistro) {
        this.fechaDeRegistro = fechaDeRegistro;
    }

    public byte getCantInfantes() {
        return cantInfantes;
    }

    public void setCantInfantes(byte cantInfantes) {
        this.cantInfantes = cantInfantes;
    }

    public byte getCantAdolencentes() {
        return cantAdolencentes;
    }

    public void setCantAdolencentes(byte cantAdolencentes) {
        this.cantAdolencentes = cantAdolencentes;
    }

    @Override
    public String toString() {
        return cedula + "\t\t" + nombre + "\t" + "\t" + email + "\t \t \t" + fechaDeRegistro + "\t" + cantInfantes + "\t" + cantAdolencentes;
    }

    public int getPrioridad() {
        return this.fechaDeRegistro.toSecondOfDay();

    }

}
